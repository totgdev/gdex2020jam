﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractPrompt : MonoBehaviour {

    float timer = 0;
    float fadeSpeed = 8;
    Text image;

	// Use this for initialization
	void Start () {
        image = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        bool show = PlayerController.instance.InDialogueRange || Fridge.FridgePrompt || GetKeys.KeyPrompt;
        timer = Mathf.Clamp01(timer + Time.deltaTime * fadeSpeed * (show ? 1 : -1));
        if (PlayerController.instance.InDialogue)
            timer = 0;
        image.color = new Color(1, 1, 1, timer);
    }
}
