﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class StartMenu : MonoBehaviour 
{
    public Image fadingBlack;
    public PlayerController player;
    public Camera startMenuCamera;
    bool started = false;
    public GameObject UIButtons;

    float fadeTime = 3;
    float fadeInTimer = 0;
    float fadeOutTimer = 0;

	// Use this for initialization
	void Start () 
    {
        player.DisablePlayerControl();
        EventSystem.current.SetSelectedGameObject(UIButtons.transform.GetChild(0).gameObject);
	}

	// Update is called once per frame
	void Update ()
    {
        if (started)
        {
            if (fadeInTimer == fadeTime)
            {
                startMenuCamera.enabled = false;
            }
            if (fadeInTimer < fadeTime)
            {
                fadeInTimer += Time.deltaTime;
                if (fadeInTimer > fadeTime) fadeInTimer = fadeTime;
                fadingBlack.color = new Color(fadingBlack.color.r, fadingBlack.color.g, fadingBlack.color.b, fadeInTimer / fadeTime);
            }
            else if (fadeInTimer == fadeTime && fadeOutTimer < fadeTime)
            {
                fadeOutTimer += Time.deltaTime;
                if (fadeOutTimer > fadeTime) fadeOutTimer = fadeTime;
                fadingBlack.color = new Color(fadingBlack.color.r, fadingBlack.color.g, fadingBlack.color.b, (fadeTime - fadeOutTimer) / fadeTime);
            }
            if (fadeOutTimer == fadeTime)
            {
                player.EnablePlayerControl();
                this.enabled = false;
            }
        }
        else
        {
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                EventSystem.current.SetSelectedGameObject(EventSystem.current.firstSelectedGameObject);
            }
        }
	}

    public void StartButton()
    {
        UIButtons.SetActive(false);
        started = true;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
