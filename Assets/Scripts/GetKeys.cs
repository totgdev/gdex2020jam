﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetKeys : MonoBehaviour
{

    public static bool KeyPrompt;

    bool playerInRange = false;
    bool done = false;

    PlayerController player;

    public Transform warpPlayerPos;
    SpriteRenderer sr;
	// Use this for initialization
	void Start () {
        sr = GetComponent<SpriteRenderer>();
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (player == null)
                player = other.GetComponent<PlayerController>();
            playerInRange = true;
            KeyPrompt = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerInRange = false;
            KeyPrompt = false;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (!done)
        {
            if (playerInRange)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    player.DispossessCharacter();
                    player.WarpPosition(warpPlayerPos);
                    StateMaster.SetVal("GotKeys", true);
                    KeyPrompt = false;
                    sr.enabled = false;
                    done = true;
                }
            }
        }
	}
}
