﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFunctions : MonoBehaviour
{
    public static Image fadingBlack;

    float fadeTime = 3;
    static float fadeInTimer = 0;
    static float fadeOutTimer = 0;

    bool fadeOnStart = false;
    static bool fadingIn = false;
    static bool fadingOut = false;

    // Use this for initialization
    void Awake ()
    {
        fadingBlack = transform.Find("FadeBlack").GetComponent<Image>();
	}

    public static void FadeIn()
    {
        fadingIn = true;
        fadeInTimer = 0;
        fadingBlack.color = new Color(fadingBlack.color.r, fadingBlack.color.g, fadingBlack.color.b, 1);
    }

    public static void FadeOut()
    {
        fadingOut = true;
        fadeOutTimer = 0;
        fadingBlack.color = new Color(fadingBlack.color.r, fadingBlack.color.g, fadingBlack.color.b, 0);
    }

    // Update is called once per frame
    void Update ()
    {
        if (fadingIn)
        {
            fadeInTimer += Time.deltaTime;
            fadingBlack.color = new Color(fadingBlack.color.r, fadingBlack.color.g, fadingBlack.color.b, (fadeTime - fadeInTimer) / fadeTime);
            if (fadeInTimer >= fadeTime)
            {
                fadingBlack.color = new Color(fadingBlack.color.r, fadingBlack.color.g, fadingBlack.color.b, 0);
                fadingIn = false;
            }
        }
        if (fadingOut)
        {
            fadeOutTimer += Time.deltaTime;
            fadingBlack.color = new Color(fadingBlack.color.r, fadingBlack.color.g, fadingBlack.color.b, fadeOutTimer / fadeTime);
            if (fadeOutTimer >= fadeTime)
            {
                fadingBlack.color = new Color(fadingBlack.color.r, fadingBlack.color.g, fadingBlack.color.b, 1);
                fadingOut = false;
            }
        }
    }
}
