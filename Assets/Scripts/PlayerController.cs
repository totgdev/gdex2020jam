﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public enum PossessedCharacter{ 
        DemonDrummer = 0,
        GoblinDave = 1,
        Frog = 2,
        Rat = 3,
        Whale = 4,
        None = 5
    }

    public static PlayerController instance;
    CharacterController controller;
    public float walkSpeed = 10;
    public float jumpHeight = 10;
    public float gravity = 9.87f;
    protected float verticalSpeed = 0;

    public float mouseSensitivity = 2f;

    public bool fadeOnStart = false;

    PossessedCharacter character = PossessedCharacter.None;
    public PossessedCharacter Character { get { return character; } }

    public GameObject basicArt;//Art of the Demon Drummer. Needs to be turned off when possessing and on when no one is possessed.
    public Transform mainCameraTransform;
    public Transform baseCameraAttach;
    protected CameraControl camControl;
    public Transform possessedTransform;
    public List<Transform> followers;//Basically a bucket for former followers to go in. They should vaguely follw the player around.
    protected Vector3 followWayPoint;//Plop something in here every few seconds for followers to move toward.
    protected float followWaypointTimer = .25f;

    protected PossessedCharacter dialoguePossessedID;
    protected Transform cameraDialogueTransform;
    protected Transform playerPosInDailogue;
    protected Transform dialogueShiftPlayerPos;
    public Transform CameraDialgoueTransform { get { return cameraDialogueTransform; } set { cameraDialogueTransform = value; } }
    protected Transform dialogueNPCTransform;
    protected Transform dialogueNPCCameraAttach;
    protected float cameraHeightAdjustment;
    protected float npcColliderHeight;
    protected float npcColliderRadius;
    protected Collider dialogueNPCTrigger;
    protected AnimationClip possessedAnim;//Does no need to be assigned.
    protected SpriteAutoCameraFace dialogueNPCAutoFace;
    protected bool dispossessOnDialogEnd = false;
    protected TalkingNPC startConvoImmediatelyAfter;
    protected bool shiftingPos = false;
    protected bool inDialogRange;
    protected bool inDialogue;
    public bool InDialogue { get { return inDialogue; } }
    public bool InDialogueRange { get { return inDialogRange; } }

    AudioSource audioSource;
    AudioClip jumpSound;

    bool playerControlEnabled = true;

	// Use this for initialization
	void Start () 
    {
        instance = this;
        controller = GetComponent<CharacterController>();
        mainCameraTransform.position = baseCameraAttach.position;
        camControl = mainCameraTransform.GetComponent<CameraControl>();

        followers = new List<Transform>();

        audioSource = GetComponent<AudioSource>();
        jumpSound = Resources.Load<AudioClip>("Sounds/sfx/Jump");


        dialoguePossessedID = PossessedCharacter.None;

        if (fadeOnStart)
        {
            UIFunctions.FadeIn();
        }

        //TODO: Need to load possessed character based on saved info
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (playerControlEnabled)
        {
            if (!inDialogue)
            {
                Locomotion();
                Rotation();
            }

            if (inDialogue)
            {
                if (DialogueUI.Run())
                {
                    if (dialoguePossessedID != PossessedCharacter.None)
                    {
                        Debug.Log("Possessed ID: " + dialoguePossessedID.ToString());
                        PossessCharacter(dialogueNPCTransform, dialoguePossessedID);
                    }
                    else
                    {
                        SetDefaultCameraPosAndRot();
                    }
                    inDialogue = false;
                    if (dispossessOnDialogEnd)
                        DispossessCharacter();
                    if (dialogueShiftPlayerPos != null)
                    {
                        StartCoroutine(ShiftPosAfterDialogue());
                    }
                    DumpDialogueVars();
                    if (startConvoImmediatelyAfter != null)
                    {
                        SetUpDialogueVars(startConvoImmediatelyAfter);
                        SetUpCameraComposition();
                        inDialogue = true;
                    }
                }
            }
            else if (inDialogRange)
            {
                if (Input.GetMouseButtonDown(0) && controller.isGrounded && !shiftingPos)
                {
                    SetUpCameraComposition();
                    inDialogue = true;
                }
            }
        }
	}

    public void DisablePlayerControl()
    {
        playerControlEnabled = false;
    }

    public void EnablePlayerControl()
    {
        playerControlEnabled = true;
    }

    public void Locomotion()
    {
        if (!shiftingPos)
        {
            //Input
            float horizontalMove = Input.GetAxis("Horizontal");
            float verticalMove = Input.GetAxis("Vertical");

            //Gravity
            verticalSpeed -= gravity * Time.deltaTime;

            //jump
            if (character == PossessedCharacter.Frog)
                if (controller.isGrounded)
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        verticalSpeed = jumpHeight;
                        audioSource.PlayOneShot(jumpSound);
                    }

            if (character != PossessedCharacter.Whale)
            {
                Vector3 gravityMove = new Vector3(0, verticalSpeed, 0);
                Vector3 move = transform.forward * verticalMove + transform.right * horizontalMove;
                controller.Move(new Vector3((move.x + gravityMove.x) * walkSpeed, move.y + gravityMove.y, (move.z + gravityMove.z) * walkSpeed) * Time.deltaTime);
            }
            else
            {
                Vector3 move = transform.forward * verticalMove + transform.right * horizontalMove;
                controller.Move(new Vector3((move.x) * walkSpeed, move.y + walkSpeed, (move.z) * walkSpeed) * Time.deltaTime);
            }

            if (followers.Count > 0 && (horizontalMove != 0 || verticalMove != 0))
                ProcessFollowers();
        }
    }

    public void Rotation()
    {
        float horizontalRot = Input.GetAxis("Mouse X");
        float verticalRot = Input.GetAxis("Mouse Y");

        transform.Rotate(0, horizontalRot * mouseSensitivity, 0);
    }

    public void PossessCharacter(Transform t, PossessedCharacter possessedID)
    {
        Debug.Log("POSSESS: " + possessedID.ToString());
        dialogueNPCTrigger.enabled = false;
        camControl.UseMouseRotation = true;
        mainCameraTransform.SetParent(transform);
        mainCameraTransform.SetPositionAndRotation(transform.position, transform.rotation);
        transform.position = t.position;
        transform.Rotate(transform.forward);
        dialogueNPCTransform.rotation = transform.rotation;
        mainCameraTransform.SetParent(transform);
        mainCameraTransform.SetPositionAndRotation(dialogueNPCCameraAttach.position, transform.rotation);

        //Switch anim if necessary
        if (possessedAnim != null)
        {
            Animator npcAnimator;
            npcAnimator = dialogueNPCTransform.GetComponent<Animator>();
            AnimatorOverrideController aoc = new AnimatorOverrideController(npcAnimator.runtimeAnimatorController);

            var anims = new List<KeyValuePair<AnimationClip, AnimationClip>>();
            foreach (var a in aoc.animationClips)
                anims.Add(new KeyValuePair<AnimationClip, AnimationClip>(a, possessedAnim));
            aoc.ApplyOverrides(anims);
            npcAnimator.runtimeAnimatorController = aoc;
        }

        //Dispossess character if you are already possessing another character when this happens.
        if (possessedTransform != null)
            DispossessCharacter();
        
        possessedTransform = t;
        t.SetParent(transform);

        controller.height = npcColliderHeight;
        controller.radius = npcColliderRadius;

        basicArt.SetActive(false);
        if(dialogueNPCAutoFace)
            dialogueNPCAutoFace.enabled = false;
        character = possessedID;
        inDialogRange = false;
        DumpDialogueVars();
        SetUpPossessedAesthetics();
    }

    public void DispossessCharacter()
    {
        Debug.Log("DISPOSSESS");
        camControl.UseMouseRotation = true;
        mainCameraTransform.SetParent(transform);
        mainCameraTransform.SetPositionAndRotation(baseCameraAttach.position, transform.rotation);
        //Put any current possessed character into the followers bucket. This will not work if re-possession is possible, but it's not designed that way rn.
        //followers.Add(possessedTransform);
        possessedTransform.GetComponent<SpriteAutoCameraFace>().enabled = true;
        if(dialogueNPCAutoFace !=null)
            dialogueNPCAutoFace.enabled = true;
        possessedTransform.SetParent(null);
        possessedTransform = null;

        controller.height = 2;
        controller.radius = .5f;

        basicArt.SetActive(true);

        character = PossessedCharacter.None;
        followWayPoint = transform.position;
        SetUpPossessedAesthetics();
    }

    //When a player enters a TalkingNPCs talk trigger, load all these variables. Dump them with a different function on exitting the trigger.
    public void SetUpDialogueVars(TalkingNPC npc)
    {
        dialoguePossessedID = npc.possessedID;
        cameraDialogueTransform = npc.CameraDialogueTransform;
        playerPosInDailogue = npc.playerPosInDailogue;
        DialogueUI.line = npc.dialogueLine;
        DialogueUI.sheet = npc.dialogueSheet;
        inDialogRange = true;
        possessedAnim = npc.newAnimationOnPossess;
        dialogueShiftPlayerPos = npc.shiftPlayerPos;

        //THIS IS FUCKEEEDD. Basically done to make sure that the Kat convo sets the possession variables for dave instead of her while keeping her convo variables set up. God we need to make sure we talk more about mechanics assumptions next jam.
        if (npc.npcTransformToPossess == null)
            dialogueNPCTransform = npc.transform;
        else
        {
            dialogueNPCTransform = npc.npcTransformToPossess;
            npc = dialogueNPCTransform.GetComponent<TalkingNPC>();
        }
        dialogueNPCCameraAttach = npc.cameraAttach;
        npcColliderHeight = npc.npcColliderHeight;
        npcColliderRadius = npc.npcColliderRadius;
        dialogueNPCAutoFace = npc.GetComponent<SpriteAutoCameraFace>();
        dialogueNPCTrigger = npc.TalkTrigger;
        dispossessOnDialogEnd = npc.dispossessOnDialogueEnd;
        startConvoImmediatelyAfter = npc.startConvoImmediatelyAfter;
    }

    public void DumpDialogueVars()
    {
        dialoguePossessedID = PossessedCharacter.None;
        cameraDialogueTransform = null;
        playerPosInDailogue = null;
        dialogueNPCTransform = null;
        dialogueNPCCameraAttach = null;
        npcColliderHeight = 2;
        npcColliderRadius = .5f;
        dialogueNPCAutoFace = null;
        dialogueNPCTrigger = null;
        inDialogRange = false;
        possessedAnim = null;
        dispossessOnDialogEnd = false;
    }

    public void DropDialogue()
    {
        inDialogRange = false;
    }

    //Move camera appropriatel to view the conversation from third person.
    public void SetUpCameraComposition()
    {
        //Do I need to detach the camera from the parent?
        camControl.UseMouseRotation = false;
        mainCameraTransform.SetParent(null);
        mainCameraTransform.SetPositionAndRotation(cameraDialogueTransform.position, cameraDialogueTransform.rotation);
        transform.SetPositionAndRotation(playerPosInDailogue.position, playerPosInDailogue.rotation);
    }

    public void SetDefaultCameraPosAndRot()
    {
        camControl.UseMouseRotation = true;
        mainCameraTransform.SetParent(transform);
        mainCameraTransform.SetPositionAndRotation(baseCameraAttach.position, transform.rotation);
    }

    public void ProcessFollowers()
    {
        followWaypointTimer -= Time.deltaTime;
        if (followWaypointTimer <= 0)
        {
            followWayPoint = transform.position;
            followWaypointTimer = .25f;
        }
        for (int i = 0; i < followers.Count; i++)
        {
            if ((followers[i].position - followWayPoint).magnitude > 2 * i + 1)
            {
                Vector3 moveVec = (followWayPoint - followers[i].position).normalized;
                followers[i].Translate(moveVec * .25f, Space.World);
            }
        }
    }

    //TODO: Set up UI, music. sfx for a possessed character
    public void SetUpPossessedAesthetics()
    {
        
    }
    //TODO: Need a way to eject from the possessed character

    public void WarpPosition(Transform warpPos)
    {
        dialogueShiftPlayerPos = warpPos;
        StartCoroutine(ShiftPosAfterDialogue());
    }

    IEnumerator ShiftPosAfterDialogue()
    {
        shiftingPos = true;
        UIFunctions.FadeOut();
        yield return new WaitForSeconds(3);
        transform.position = dialogueShiftPlayerPos.position;
        UIFunctions.FadeIn();
        shiftingPos = false;
    }
}
