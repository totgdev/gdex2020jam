﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scenetransition_statebased : MonoBehaviour
{
    public string[] statesToSwitchScene;
    public string sceneToSwitchTo;
    bool transitioning = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!transitioning)
        {
            foreach (string s in statesToSwitchScene)
            {
                if (StateMaster.LoadVal(s, false) == true)
                    continue;
                else
                    return;
            }

            StartCoroutine(WaitThenSwitchScene());
        }
	}

    IEnumerator WaitThenSwitchScene()
    {
        transitioning = true;
        UIFunctions.FadeOut();
        yield return new WaitForSeconds(3.1f);
        SceneManager.LoadScene(sceneToSwitchTo);
    }
}
