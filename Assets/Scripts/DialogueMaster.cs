﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DialogueMaster
{
    public static Dictionary<string, DialogueSheet> DialogueSheets;
    public static string ResourcePath = "Dialogue";
    public static string FolderPath { get { return Application.dataPath + "/Resources/" + ResourcePath + "/"; } }
    static DialogueMaster()
    {
        DialogueSheets = new Dictionary<string, DialogueSheet>();
        Object[] objs = Resources.LoadAll(ResourcePath);
        foreach (Object obj in objs)
        {
            TextAsset sheet = (TextAsset)obj;
            string sheetName = sheet.name.Split('.')[0];
            DialogueSheets.Add(sheetName, DialogueSheet.LoadSheet(sheetName, sheet.text));
        }
    }

    public static DialogueSheet GetSheet(string sheetName)
    {
        if (!DialogueSheets.ContainsKey(sheetName))
            Debug.LogError("Could not find sheet " + sheetName);
        return DialogueSheets[sheetName];

    }

    public static DialogueLine GetLine(string sheetName, string lineLabel)
    {
        DialogueSheet sheet = GetSheet(sheetName);
        return sheet[sheet.GetIndex(lineLabel)];
    }
}

public class DialogueLabel
{
    public string sheet;
    public string label;

    public DialogueLabel()
    {
        sheet = "";
        label = "";
    }

    public DialogueLabel(string sheet, string label)
    {
        this.sheet = sheet;
        this.label = label;
    }
}

public class DialogueSheet
{
    public DialogueLine this[int index] { get { return lines[index]; } }
    DialogueLine[] lines;
    public string name;

    public static DialogueSheet LoadSheet(string name, string text)
    {
        //Debug.Log("Processing sheet " + name);
        text = text.Replace("\r", "");
        List<List<string>> rawList = new List<List<string>>();
        int index = 0;
        string currentString = "";
        bool withinQuote = false;
        List<string> rawFields = new List<string>();
        while (true)
        {
            if (text[index] == ',')
            {
                if (withinQuote)
                    currentString += ',';
                else
                {
                    rawFields.Add(currentString);
                    currentString = "";
                }
            }
            else if (text[index] == '"')
            {
                withinQuote = !withinQuote;
                if (index != 0 && text[index - 1] == '"')
                    currentString += '"';
            }
            else if (text[index] == '\n')
            {
                if (withinQuote)
                    currentString += '\n';
                else
                {
                    rawFields.Add(currentString);
                    currentString = "";
                    rawList.Add(rawFields);
                    rawFields = new List<string>();
                }
            }
            else
            {
                currentString += text[index];
            }
            index++;
            if (index == text.Length)
            {
                rawFields.Add(currentString);
                rawList.Add(rawFields);
                break;
            }
        }
        List<DialogueLine> lineList = new List<DialogueLine>();
        index = 0;
        foreach (List<string> rawLine in rawList)
        {
            if (rawLine.Count == 0)
                continue;
            DialogueLine dl = new DialogueLine(index, rawLine);
            if (dl.body == null || dl.body.Length == 0)
                continue;
            lineList.Add(dl);
            index++;
        }
        if (!lineList[lineList.Count - 1].isEnd)
            Debug.LogError(name + " sheet does not end with an END");
        DialogueSheet ds = new DialogueSheet();
        ds.name = name;
        ds.lines = lineList.ToArray();
        foreach (DialogueLine line in ds.lines)
        {
            line.SheetProcess(ds);
        }
        return ds;
    }

    public int GetIndex(string label)
    {
        label = label.ToLower();
        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].label == label)
                return i;
        }
        Debug.LogError("Could not find line " + label + " in sheet " + name);
        return -1;
    }
}

public class DialogueLine
{
    public int index;
    public Dictionary<string, string> keyItems;
    public List<string> items;

    public string label;
    public string body;
    public string speaker = "player";
    public string state;
    public bool isEnd;
    public bool isChoice;
    public bool isNext;
    public string[] choices;
    public DialogueLine[] nextLines;

    public DialogueLine(int index, List<string> rawLine)
    {
        this.index = index;
        keyItems = new Dictionary<string, string>();
        items = new List<string>();
        foreach (string line in rawLine)
        {
            string[] lineSplit = line.Split(':');
            if (lineSplit.Length > 2)
                Debug.LogError("Too many ':' characters in entry " + line);
            if (lineSplit.Length > 1)
                keyItems.Add(lineSplit[0].Trim(), lineSplit[1].Trim());
            else
                items.Add(line.Trim());
        }
        foreach (string item in items)
        {
            switch (item.ToLower())
            {
                case "end":
                    isEnd = true;
                    break;
            }
        }
        foreach (string key in keyItems.Keys)
        {
            string value = keyItems[key];
            switch (key.ToLower())
            {
                case "label":
                    label = value.ToLower();
                    break;
                case "body":
                    body = value;
                    break;
                case "speaker":
                    speaker = value;
                    break;
                case "state":
                    state = value;
                    break;
            }
        }
    }

    public void SheetProcess(DialogueSheet sourceSheet)
    {
        foreach (string item in items)
        {
            switch (item)
            {
            }
        }
        foreach (string key in keyItems.Keys)
        {
            string value = keyItems[key];
            switch (key.ToLower())
            {
                case "choice":
                    isChoice = true;
                    string[] args = value.Split(',');
                    List<string> choiceList = new List<string>();
                    List<DialogueLine> nextLineList = new List<DialogueLine>();
                    if (args.Length % 2 != 0)
                        Debug.LogError("Line " + body + " from sheet " + sourceSheet.name + " has bad choice args");
                    for (int i = 0; i < args.Length; i += 2)
                    {
                        choiceList.Add(args[i].Trim());
                        nextLineList.Add(sourceSheet[sourceSheet.GetIndex(args[i + 1].Trim())]);
                    }
                    choices = choiceList.ToArray();
                    nextLines = nextLineList.ToArray();
                    break;
                case "next":
                    nextLines = new DialogueLine[] { sourceSheet[sourceSheet.GetIndex(value.Trim())] };
                    break;
            }
        }
        if (!isChoice && !isEnd)
        {
            isNext = true;
            if (nextLines == null)
                nextLines = new DialogueLine[] { sourceSheet[index + 1] };
        }
    }
}