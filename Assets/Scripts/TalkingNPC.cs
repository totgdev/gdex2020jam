﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkingNPC : MonoBehaviour
{
    Collider talkTrigger;
    public Collider TalkTrigger { get { return talkTrigger; } }
    PlayerController player;

    public PlayerController.PossessedCharacter possessedID;
    public Transform npcTransformToPossess;//If blank, use your own transform
    public Transform CameraDialogueTransform;
    public Transform playerPosInDailogue;

    public string dialogueSheet;
    //public string dialogueLabel;

    public DialogueLine dialogueLine;
    public Transform cameraAttach;
    public float npcColliderRadius;
    public float npcColliderHeight;
    public PlayerController.PossessedCharacter willOnlyTalkTo;

    public string[] requiredConvos;
    public string[] illegalConvos;
    public AnimationClip newAnimationOnPossess;
    public AudioClip talkclip;
    AudioSource aSource;

    SpriteRenderer sr;
    Animator theAnimator;

    public bool dispossessOnDialogueEnd = false;
    //Where to shift the player to after the convo. Does nothing if null
    public Transform shiftPlayerPos;
    //Starts another convo immediately after this one ends. 
    public TalkingNPC startConvoImmediatelyAfter;
    bool exist;
    public bool Exist { get { return exist; } }

    // Use this for initialization
    void Start () {
        talkTrigger = GetComponent<Collider>();
        //Debug.Log("Missing Sheet", this);
        dialogueLine = DialogueMaster.GetLine(dialogueSheet, "start");
        sr = GetComponent<SpriteRenderer>();
        aSource = gameObject.AddComponent<AudioSource>();
        aSource.spatialBlend = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!exist)
            return;
        if (other.gameObject.CompareTag("Player"))
        {
            if (player == null)
                player = other.gameObject.GetComponent<PlayerController>();
            if(willOnlyTalkTo == PlayerController.PossessedCharacter.DemonDrummer || willOnlyTalkTo == PlayerController.PossessedCharacter.None)
                player.SetUpDialogueVars(this);
            else if (player.Character == willOnlyTalkTo)
            { 
                player.SetUpDialogueVars(this);
                if (talkclip != null)
                {
                    //aSource.Stop();
                    aSource.PlayOneShot(talkclip);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!exist)
            return;
        if (other.gameObject.CompareTag("Player"))
        {
            if (player == null)
                player = other.gameObject.GetComponent<PlayerController>();
            player.DropDialogue();

        }
    }

    // Update is called once per frame
    //Turn on 
    void Update () {
        exist = true;
        foreach (string convo in requiredConvos)
        {
            if (!StateMaster.LoadVal(convo, false))
            {
                exist = false;
                break;
            }
        }
        foreach (string convo in illegalConvos)
        {
            if (StateMaster.LoadVal(convo, false))
            {
                exist = false;
                if (talkTrigger != null)
                    talkTrigger.enabled = exist;
                break;
            }
        }
        sr.enabled = exist;
    }

    public void SwitchDialogSheet(string sheetName)
    {
        dialogueSheet = sheetName;
        dialogueLine = DialogueMaster.GetLine(dialogueSheet, "start");
    }

    //Needed in case one conva possesses but another does nto on the same chararacter
    public void SwitchPossessedID(PlayerController.PossessedCharacter newID)
    {
        possessedID = newID;
    }

    public void SwitchAnimationClip()
    {
        if (newAnimationOnPossess != null)
        {
            Animator npcAnimator;
            if (npcTransformToPossess != null)
                npcAnimator = npcTransformToPossess.GetComponent<Animator>();
            else
                npcAnimator = GetComponent<Animator>();
            AnimatorOverrideController aoc = new AnimatorOverrideController();

            var anims = new List<KeyValuePair<AnimationClip, AnimationClip>>();
            foreach (var a in aoc.animationClips)
                anims.Add(new KeyValuePair<AnimationClip, AnimationClip>(a, newAnimationOnPossess));
            aoc.ApplyOverrides(anims);
            npcAnimator.runtimeAnimatorController = aoc;
        }
    }
}
