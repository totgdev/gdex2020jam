﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueUI : MonoBehaviour {

    public static DialogueUI instance;
    public static DialogueLine line;
    public static string sheet;
    static bool choosing = false;
    static int choiceIndex = 0;


    Text text;
    Image image;
    GameObject choiceIndicators;

	// Use this for initialization
	void Start () {
        instance = this;
        text = transform.GetChild(1).GetComponent<Text>();
        image = GetComponent<Image>();
        choiceIndicators = transform.GetChild(0).gameObject;
        Disable();
	}

    public static void SetSpeaker(string speaker)
    {
        if (!SpeechSource.sources.ContainsKey(speaker.ToLower()))
            return;
        SpeechSource speecher = null;
        foreach (SpeechSource s in SpeechSource.sources[speaker.ToLower()])
        {
            if (s != null && s.Valid())
            {
                speecher = s;
                break;
            }
        }
        if (speecher == null)
            return;
        Vector3 pos = Camera.main.WorldToScreenPoint(speecher.transform.position);
        pos.x = Mathf.Clamp(pos.x + speecher.pixelOffset.x, 190, Screen.width - 190);
        pos.y = Mathf.Clamp(pos.y + speecher.pixelOffset.y, 120, Screen.height - 120);
        pos.z = 0;
        instance.transform.position = pos;
    }

    public static void Enable()
    {
        instance.text.enabled = true;
        instance.image.enabled = true;
    }

    public static void Disable()
    {
        instance.text.enabled = false;
        instance.image.enabled = false;
    }

    public static void ChoiceDisable()
    {
        instance.choiceIndicators.SetActive(false);
    }

    public static void ChoiceEnable()
    {
        instance.choiceIndicators.SetActive(true);
    }

    public static bool Run()
    {
        Enable();
        if (!choosing)
        {
            ChoiceDisable();
            DialogueUI.SetSpeaker(line.speaker);
            if (Input.GetMouseButtonDown(0))
            {
                if (line.isEnd)
                {
                    Disable();
                    StateMaster.SetVal(sheet, true);
                    return true;
                }
                else if (line.isChoice)
                {
                    choosing = true;
                    return false;
                }
                else
                {
                    line = line.nextLines[0];
                }
            }
            instance.text.text = line.body;
        }
        if (choosing)
        {
            ChoiceEnable();
            DialogueUI.SetSpeaker("YOU");
            if (Input.GetMouseButtonDown(0))
            {
                line = line.nextLines[choiceIndex];
                choiceIndex = 0;
                choosing = false;
                return false;
            }
            if (Input.GetKeyDown(KeyCode.A))
                choiceIndex = (choiceIndex - 1 + line.nextLines.Length) % line.nextLines.Length;
            if (Input.GetKeyDown(KeyCode.D))
                choiceIndex = (choiceIndex + 1) % line.nextLines.Length;
            instance.text.text = line.choices[choiceIndex];

        }
        return false;
    }

}
