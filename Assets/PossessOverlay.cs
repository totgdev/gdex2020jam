﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PossessOverlay : MonoBehaviour {

    public PlayerController.PossessedCharacter identity;
    Text image;

	// Use this for initialization
	void Start () {
        image = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if (PlayerController.instance.Character == identity && !PlayerController.instance.InDialogue)
            image.color = new Color(0, 0, 0, 191f/255f);
        else
            image.color = Color.clear;
	}
}
