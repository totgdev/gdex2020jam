﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/WaterShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Color("Color", Color) = (0,0,0,0)
	}

	SubShader
	{
		Blend One One
		ZWrite Off
		Cull Off

		Tags
		{
			"RenderType" = "Transparent"
			"Queue" = "Transparent"
		}

		Pass
		{
			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 screenuv : TEXCOORD1;
				float3 viewDir : TEXCOORD2;
				float3 objectPos : TEXCOORD3;
				float4 vertex : SV_POSITION;
				float depth : DEPTH;
				float3 normal : NORMAL;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);

				o.screenuv = ((o.vertex.xy / o.vertex.w) + 1) / 2;
				o.screenuv.y = 1 - o.screenuv.y;
				o.depth = -mul(UNITY_MATRIX_MV, v.vertex).z * _ProjectionParams.w;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				return o;
			}

			sampler2D _CameraDepthNormalsTexture;
			fixed4 _Color;
			
			fixed4 frag(v2f i) : SV_Target
			{
				float screenDepth = DecodeFloatRG(tex2D(_CameraDepthNormalsTexture, i.screenuv).zw);
				float diff = screenDepth - i.depth;
				float intersect = pow((2 * (0.5 - abs(tex2D(_MainTex, i.uv
					+ sin(_Time[1] + i.uv.x) * float2(0.004, 0.01)
					+ sin((_Time[1] + 8*i.uv.y) * 3) * float2(0.0005, 0.002)).r - 0.5)))
					, 12 + 6 * sin(3*_Time[1] + 10*(i.uv.x * i.uv.y)));

				if (diff > 0)
					intersect += 1 - smoothstep(0, _ProjectionParams.w, diff);

				fixed4 col = _Color * _Color.a + intersect;
				col.a = 1;
				return col;
			}
			ENDCG
		}
	}
}
