﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMaster {
    public static Dictionary<string, float> floatValues;
    public static Dictionary<string, string> stringValues;
    public static Dictionary<string, bool> boolValues;

    static StateMaster ()
    {
        floatValues = new Dictionary<string, float>();
        stringValues = new Dictionary<string, string>();
        boolValues = new Dictionary<string, bool>();
    }

    public static void SetVal(string label, string value)
    {
        stringValues[label] = value;
    }

    public static void SetVal(string label, float value)
    {
        floatValues[label] = value;
    }

    public static void SetVal(string label, bool value)
    {
        boolValues[label] = value;
    }

    public static string LoadVal(string label, string defaultValue)
    {
        if (stringValues.ContainsKey(label))
            return stringValues[label];
        stringValues[label] = defaultValue;
        return defaultValue;
    }

    public static float LoadVal(string label, float defaultValue)
    {
        if (floatValues.ContainsKey(label))
            return floatValues[label];
        floatValues[label] = defaultValue;
        return defaultValue;
    }

    public static bool LoadVal(string label, bool defaultValue)
    {
        if (boolValues.ContainsKey(label))
            return boolValues[label];
        boolValues[label] = defaultValue;
        return defaultValue;
    }

    public static void HardReset()
    {
        floatValues = new Dictionary<string, float>();
        stringValues = new Dictionary<string, string>();
        boolValues = new Dictionary<string, bool>();
    }
}
