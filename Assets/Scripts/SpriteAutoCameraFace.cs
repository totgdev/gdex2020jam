﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAutoCameraFace : MonoBehaviour {

    GameObject mainCam;

	// Use this for initialization
	void Start () 
    {
        mainCam = GameObject.FindGameObjectWithTag("MainCamera");
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 mainCamNoY = new Vector3(mainCam.transform.position.x, transform.position.y, mainCam.transform.position.z);
        Vector3 yrotation = (mainCamNoY - transform.position);
        transform.rotation = Quaternion.LookRotation(yrotation);
	}
}
