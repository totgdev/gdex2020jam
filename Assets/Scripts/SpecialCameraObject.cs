﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialCameraObject : MonoBehaviour
{
    public Transform cameraLookTransform;
    PlayerController control;
    // Use this for initialization
    void Start () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (control == null)
            {
                if (other.GetComponent<PlayerController>() != null)
                    control = other.GetComponent<PlayerController>();
            }
            if (control != null)
            {
                control.CameraDialgoueTransform = cameraLookTransform;
                control.SetUpCameraComposition();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if(control == null)
                control = other.GetComponent<PlayerController>();
            if(control != null)
                control.SetDefaultCameraPosAndRot();
        }
    }
}
