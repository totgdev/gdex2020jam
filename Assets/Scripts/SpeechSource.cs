﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechSource : MonoBehaviour {

    public static Dictionary<string, List<SpeechSource>> sources = new Dictionary<string, List<SpeechSource>>();

    public string name;
    TalkingNPC tn;

    [HideInInspector]
    public Vector2 pixelOffset;

    void Start()
    {
        tn = GetComponent<TalkingNPC>();
        pixelOffset = Vector2.up * 250;
        if (!sources.ContainsKey(name.ToLower()))
            sources[name.ToLower()] = new List<SpeechSource>();
        sources[name.ToLower()].Add(this);
    }

    public bool Valid() {
        if (tn == null)
            return true;
        if (tn.Exist)
            return true;
        return false;
    }

}
