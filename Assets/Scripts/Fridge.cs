﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fridge : MonoBehaviour
{

    public static bool FridgePrompt = false;

    bool opening = false;
    bool fridgeOpen = false;
    bool playerInRange = false;
    public Transform fridgeDoor;
    public GameObject[] beers;

    bool done = false;

	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!done && StateMaster.LoadVal("DavePossess", false))
                FridgePrompt = true;
            playerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            FridgePrompt = false;
            playerInRange = false;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (playerInRange)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (StateMaster.LoadVal("DavePossess", false))
                {
                    if (!opening)
                    {
                        opening = true;
                        StartCoroutine(OpenFridgeDoor());
                    }
                    else if (fridgeOpen)
                    {
                        FridgePrompt = false;
                        done = true;
                        foreach (GameObject g in beers)
                        {
                            g.SetActive(false);
                        }
                        StateMaster.SetVal("GotBeers", true);
                    }
                }
            }
        }
	}

    IEnumerator  OpenFridgeDoor()
    {
        while (fridgeDoor.localRotation.eulerAngles.y < 90)
        {
            yield return new WaitForEndOfFrame();
            fridgeDoor.Rotate(new Vector3(0, 1f, 0));
        }
        fridgeDoor.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        fridgeOpen = true;
    }
}
