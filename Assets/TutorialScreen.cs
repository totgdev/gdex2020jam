﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialScreen : MonoBehaviour {

    float fadeIn = 2;
    float fadeOut = 2;
    float timer = 0;
    bool skipped = false;
    Image image;
    public string nextScene;

    // Use this for initialization
    void Start () {
        image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!skipped && timer < 2)
        {
            timer += Time.deltaTime;
            image.color = new Color(1, 1, 1, timer / fadeIn);
        }
        else if (!skipped && Input.GetMouseButton(0))
        {
            skipped = true;
        }
        else
        {
            timer -= Time.deltaTime;
            image.color = new Color(timer / fadeOut, timer / fadeOut, timer / fadeOut, 1);
            if (timer < 0)
                SceneManager.LoadScene(nextScene);
        }
            
	}
}
