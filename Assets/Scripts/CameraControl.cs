﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float mouseSensitivity = 2f;
    public float upLimit = -50;
    public float downLimit = 50;

    //turn off when camera needs to be fixed.
    protected bool useMouseRotation = true;
    public bool UseMouseRotation { get { return useMouseRotation; } set { useMouseRotation = value; } }
	
	// Update is called once per frame
	void Update () {
        if(useMouseRotation)
            MouseRotation();
	}

    public void MouseRotation()
    {
        float horizontalRotation = Input.GetAxis("Mouse X");
        float verticalRotation = Input.GetAxis("Mouse Y");

        transform.Rotate(-verticalRotation * mouseSensitivity, 0, 0);

        Vector3 currentRotation = transform.localEulerAngles;
        if (currentRotation.x > 180)
            currentRotation.x -= 360;
        currentRotation.x = Mathf.Clamp(currentRotation.x, upLimit, downLimit);
        transform.localRotation = Quaternion.Euler(currentRotation);
    }
}
