﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueStateSwitch : MonoBehaviour 
{
    public string[] requiredFlags;
    public PlayerController.PossessedCharacter[] newPossessed;
    public string[] newDialogSheet;
    public AnimationClip[] newAnimation;
    int progression = 0;//can go as high as the highest index for the above arrays
    TalkingNPC theNPC;

    // Use this for initialization
    void Start ()
    {
        theNPC = GetComponent<TalkingNPC>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (progression < requiredFlags.Length)
        {
            if (StateMaster.LoadVal(requiredFlags[progression], false))
            {
                theNPC.SwitchDialogSheet(newDialogSheet[progression]);
                theNPC.SwitchPossessedID(newPossessed[progression]);
                
                //This junk changes the animtionclip in the animator at runtime in like the one case where that's necessary.
                if (newAnimation.Length > progression)
                {
                    Animator npcAnimator;
                    if (theNPC.npcTransformToPossess != null)
                        npcAnimator = theNPC.npcTransformToPossess.GetComponent<Animator>();
                    else
                        npcAnimator = theNPC.GetComponent<Animator>();
                    AnimatorOverrideController aoc = new AnimatorOverrideController();

                    var anims = new List<KeyValuePair<AnimationClip, AnimationClip>>();
                    foreach (var a in aoc.animationClips)
                        anims.Add(new KeyValuePair<AnimationClip, AnimationClip>(a, newAnimation[progression]));
                    aoc.ApplyOverrides(anims);
                    npcAnimator.runtimeAnimatorController = aoc;
                }
                progression++;
            }
        }
	}
}
