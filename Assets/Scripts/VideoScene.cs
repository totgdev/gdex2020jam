﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VideoScene : MonoBehaviour {

    public float videoDuration;
    public string nextScene;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        videoDuration -= Time.deltaTime;
        if(videoDuration <= 0)
        SceneManager.LoadScene(nextScene);
	}
}
