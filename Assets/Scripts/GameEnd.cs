﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameEnd : MonoBehaviour 
{
    bool faded = false;
    AudioClip endsong;
    public AudioSource musicSource;
    // Use this for initialization
    float creditsTimer = 49;//in seconds
    float thanksFadeInTimer = 25;
    float thanksAlpha = 0;
    public GameObject creditsContainer;
    public Text thanksText;
    public PlayerController player;


	void Start () {
		endsong = Resources.Load<AudioClip>("Sounds/music/credits_track");
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!faded)
        {
            if (StateMaster.LoadVal("Finale", false))
            {
                UIFunctions.fadingBlack.color = new Color(0, 0, 0, 1);
                faded = true;
                musicSource.loop = false;
                musicSource.Stop();
                musicSource.clip = endsong;
                musicSource.Play();
                creditsContainer.SetActive(true);
                player.DisablePlayerControl();
            }
        }
        if (faded)
        {
            creditsTimer -= Time.deltaTime;
            thanksFadeInTimer -= Time.deltaTime;
            if(creditsTimer < 44)
                creditsContainer.transform.Translate(Vector3.up * 50 * Time.deltaTime);
        }
        if (thanksAlpha < 1 && thanksFadeInTimer <= 0)
        {
            thanksAlpha += Time.deltaTime;
            thanksText.color = new Color(1, 1, 1, thanksAlpha);
        }
        if (creditsTimer <= 0)
        {
            StateMaster.HardReset();
            SceneManager.LoadScene("punkhouse_test");
        }
	}
}
